<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReqLog extends Model
{
    protected $table = 'req_log';

    protected $guarded = [];
}
