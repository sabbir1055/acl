<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OTP extends Model
{
    protected $table = 'otp';
    protected $guarded = [];
    protected $hidden = ['created_at','updated_at','id'];
}
