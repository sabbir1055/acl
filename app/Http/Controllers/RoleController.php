<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index(Request $request)
    {
        $req = $request->all();
        $role = Role::whereNotIn('id',[6,7])->paginate(10);
        return view('admin.role.index',compact('role','req'));
    }

    public function create(Request $request)
    {
        $permission = Permission::pluck('name','name')->toArray();
        return view('admin.role.create',compact('permission'));
    }

    public function store(Request $request)
    {
        $role = Role::updateOrCreate([
            'name' => $request->name,
            'guard_name' => $request->guard,
        ]);
        $role->syncPermissions($request->permission);
        session()->flash('success','Role Added.');
        return redirect()->route('role.index');
    }

    public function update(Request $request,$id)
    {
        $role = Role::find($id);
        $role->update([
            'name' => $request->name,
            'guard_name' => $request->guard,
        ]);
        $role->syncPermissions($request->permission);
        updateLog('App\Role', $id, $request->all());
        session()->flash('success','Role Updated.');
        return redirect()->route('role.index');
    }

    public function edit(Request $request,$id)
    {
        $role = Role::find($id);
        $permission = Permission::whereNotIn('id',$role->permissions->pluck('id')->toArray())->pluck('name','id')->toArray();
        return view('admin.role.edit',compact('permission','role'));
    }
}
