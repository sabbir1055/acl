<?php

namespace App\Http\Controllers;

use App\ApiUser;
use App\District;
use App\Division;
use App\PostOffice;
use App\Upazila;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function division(Request $request)
    {
        $division = Division::where('status', 1)->get();
        return $division;
    }

    public function district(Request $request)
    {
        $district = District::where('status', 1);
        if ($request->filled('division_id')) {
            $district->where('division_id', $request->division_id);
        }
        $district = $district->get();
        return $district;
    }

    public function upazila(Request $request)
    {
        $upazila = Upazila::where('status', 1);
        if ($request->filled('district_id')) {
            $upazila->where('district_id', $request->district_id);
        }
        $upazila = $upazila->get();
        return $upazila;
    }
}
