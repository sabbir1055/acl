<?php

namespace App\Http\Controllers;

use App\ReqLog;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Log;
use URL;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request)
    {
        ReqLog::create([
            'guard' => 'admin',
            'path' => $request->path(),
            'req_data' => json_encode($request->all()),
            'method' => $request->method()
        ]);
        Log::info('Req Path ==> '.URL::current().'.Req Data ==> '. json_encode($request->all()));
    }
}
