<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        $req = $request->all();
        $permission = Permission::orderBy('name','asc')->paginate(10);
        return view('admin.permission.index',compact('permission','req'));
    }

    public function store(Request $request)
    {
        Permission::updateOrCreate([
            'name' => $request->name,
            'guard_name' => $request->guard
        ]);
        Session()->flash('success','Permission created');
        return redirect()->route('permission.index');
    }
    public function update(Request $request,$id)
    {
        $permission = Permission::find($id);
        $permission->update([
            'name' => $request->name,
            'guard_name' => $request->guard
        ]);
        updateLog('App\Permission', $id, $request->all());
        session()->flash('success','Permission Updated');
        return redirect()->route('permission.index');
    }
}
