<?php

namespace App\Http\Controllers\api;

use Validator;
use App\User;
use Hash;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use ApiResponseTrait;

    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required|numeric|exists:users,employee_id',
            'password' => 'required|min:8'
        ],
        [
            'password.required' => 'Password field is required',
            'password.min' => 'Password minimum length 8',
        ]);

        if ($validator->fails()) return $this->set_response(null, 422, 'error', $validator->errors()->all());
        $user = User::where('employee_id', $request->employee_id)->first();
        if( Hash::check($request->password,$user->password)){
            $token = $user->generateToken();
            return $this->set_response($user, 200, 'success', ['successfully logged in']);
        } else {
            return $this->set_response(null, 422, 'error', ['invalid username or password']);
        }
    }
}
