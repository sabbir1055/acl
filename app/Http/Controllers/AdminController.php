<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\PostOffice;
use Illuminate\Http\Request;
use Validator, Redirect, Response;
use App\User;
use Illuminate\Support\Facades\Auth;
use Session;

class AdminController extends Controller
{
    public function adminLogin(LoginRequest $request)
    {
        if (Auth::attempt(['employee_id' => $request->employee_id, 'password' => $request->password, 'status' => 1])) {
            return redirect()->intended('dashboard');
        }
        return back()->with('errorMsg', 'Oppes! You have entered invalid credentials');

    }

    public function logout()
    {
        auth()->logout();
        return Redirect('/');
    }
}
