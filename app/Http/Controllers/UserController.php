<?php

namespace App\Http\Controllers;
use App\User;
use Validator;
use Log;
use Hash;
use DB;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function system_userIndex(Request $request)
    {
        $req = $request->all();
        $users = User::with('getInfo')->whereNotIn('id', [1]);
        ($request->f_name ? $users->where('name', 'like', '%' . $request->f_name . '%') : null);
        ($request->f_msisdn ? $users->where('msisdn', 'like', '%' . $request->f_msisdn . '%') : null);
        ($request->f_role ? $users->whereHas('roles', function ($q) use ($request) {
            $q->where('name', $request->f_role);
        }) : null);
        $users = $users->paginate(10);
        return view('admin.users.system_user_list', compact('users', 'req'));
    }

    public function system_userStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'msisdn' => 'required|unique:users,msisdn',
            'image' => 'mimes:jpeg,png,gif,jpg,webp',
            'role' => 'required|exists:roles,name',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        try {
            DB::beginTransaction();
            $image = 'user.png';
            if ($request->has('image')) {
                $image = fileUpload($request->file('image'), 'users');
            }
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'msisdn' => $request->msisdn,
                'password' => '12345678',
                'image' => $image
            ]);
            $user->syncRoles([$request->role]);
            session()->flash('success', 'User successfully created!!!');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            //            session()->flash('error','User failed to create!!!');
            session()->flash('error', $e->getMessage());
        }
        return back();
    }

    public function system_userUpdate(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'msisdn' => 'required|unique:users,msisdn,' . $id,
            'role' => 'required|exists:roles,name',
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::findOrFail($id);
        $image = $user->image;
        if ($request->has('image')) {
            $image = fileUpload($request->file('image'), 'users');
        }
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'msisdn' => $request->msisdn,
            'image' => $image
        ]);
        $user->syncRoles([$request->role]);
        updateLog('App\User', $id, $request->all());
        session()->flash('success', 'User updated successfully!!!');
        return back();
    }

    public function profile()
    {
        return view('admin.profile');
    }

    public function profileUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required_with:new_password',
            'new_password' => 'min:8',
            'confirm_password' => 'min:8|same:new_password',
            'email' => 'nullable|unique:users,email,'.auth()->user()->id,
            'mobile' => 'nullable|regex:^(?:\+?+88)?01[345-9]\d{8}$^|unique:users,mobile,'.auth()->user()->id,
        ]);
        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = auth()->user();
        if ($request->filled('new_password')) {
            if (Hash::check($request->old_password, $user->password)) {
                $user->update([
                    'password' => $request->new_password
                ]);
                session()->flash('success', 'Password updated.');
                return back();
            } else {
                session()->flash('error', 'Invalid old password.');
                return back();
            }
        } else {
            $image = $user->image;
            if (isset($request->image) && $request->file('image')) {
                $image = fileUpload($request->file('image'), 'users');
                $user->update([
                    'image' => $image
                ]);
                session()->flash('success', 'Profile Picture updated.');
                return back();
            } else {
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'mobile' => $request->mobile
                ]);
                $user->getInfo()->update([
                    'present_address' => $request->present_address,
                    'parmanent_address' => $request->parmanent_address,
                ]);
                updateLog('App\User', $user->id, $request->all());
                session()->flash('success', 'Profile updated.');
                return back();
            }
        }
    }

}
