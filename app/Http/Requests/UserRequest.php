<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'parent_id' => 'required|exists:users,unique_id',
            'designation' =>'required|exists:roles,id',
            'work_area' => 'required',
            'nid' => 'required|max:17',
            'email' => 'nullable|unique:users,email|email',
            'msisdn' => 'required|unique:users,msisdn,'.$this->route('id'),
            'image_1' => 'required|mimes:jpeg,png,webp,jpg',
            'image_2' => 'required|mimes:jpeg,png,webp,jpg',
        ];
    }
}
