<?php

use App\SmsLog;
use App\UpdateLog;
use Spatie\Permission\Models\Role;


function sendSms($msisdn,$body){
    $user = "Hello_Doctor";
    $pass = "u<69I764";
    $sid = "HellodoctorbdENG";
    $unique_id = rand(00000000,99999999);
    $sms = new SmsLog();
    $sms->msisdn = $msisdn;
    $sms->body = $body;
    $sms->priority = 9;
    $sms->status = 0;
    $sms->save();
    $url = "http://sms.sslwireless.com/pushapi/dynamic/server.php";
    $param = "user=$user&pass=$pass&sms[0][0]= ".$msisdn." &sms[0][1]=" . urlencode($body) . "&sms[0][2]=" . $unique_id . "&sid=$sid";

    $crl = curl_init();
    curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($crl, CURLOPT_URL, $url);
    curl_setopt($crl, CURLOPT_HEADER, 0);
    curl_setopt($crl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($crl, CURLOPT_POST, 1);
    curl_setopt($crl, CURLOPT_POSTFIELDS, $param);
    $response = curl_exec($crl);

    $sms->status = 3;
    $sms->sms_reply = $response;
    $sms->save();

    Log::info(['SMS Info ========>' => json_encode($sms)]);
    // dd($response);
    return 'success';
}

function generateVerifyToken($length){
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $token = substr(str_shuffle($chars), 0, $length);
    return $token;
}

function getSystemRoles(){
    return Role::whereNotIn('id',[1])->pluck('name','name')->toArray();
}

function array_flatten($array) {
    if (!is_array($array)) {
        return false;
    }
    $result = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, array_flatten($value));
        } else {
            $result = array_merge($result, array($key => $value));
        }
    }
    return $result;
}

function fileUpload($query, $path){
    $image_name = time() . rand(11111, 99999);
    $ext = strtolower($query->getClientOriginalExtension()); // You can use also getClientOriginalName()
    $image_full_name = $image_name.'.'.$ext;
    $upload_path = 'uploads/'.$path;    //Creating Sub directory in Public folder to put image
    $query->move($upload_path,$image_full_name);
    return $path.'/'.$image_full_name; // Just return image
}

    //return $html;
function updateLog($model, $model_id,$data){
    $log = UpdateLog::create([
        'model_name' => $model,
        'model_id' => $model_id,
        'update_by' => auth()->user()->id,
        'update_data' => json_encode($data)
    ]);

    return $log;
}