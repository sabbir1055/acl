<?php
/**
 * Created by PhpStorm.
 * User: sabbir.hosain
 * Date: 3/16/2020
 * Time: 6:23 PM
 */

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class DesignationScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('designations.id', '!=', 1);
    }
}