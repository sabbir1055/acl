<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    protected $table = 'user_info';
    protected $guarded = [];
    protected $appends = ['gender_value'];
    public function getGenderValueAttribute(){
        $data=[
            1=> 'Male',
            2=> 'Female',
            3=> 'Others',
        ];
        return $data[$this->gender] ?? null;
    }
}
