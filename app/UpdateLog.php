<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateLog extends Model
{
    protected $table = "update_log";
    protected $guarded = [];
}
