<?php

namespace App;

use App\Models\Attendance;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    protected $guard_name = 'web';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','status','created_by','created_at','updated_by', 'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'password' => 'string',
        'status' => 'integer',
        'email_verified_at' => 'datetime',
    ];

    public function getImageAttribute($value){
        return asset('/uploads').'/'.$value;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);
    }

    public function getInfo(){
        return $this->hasOne(UserInfo::class);
    }

    public function generateToken(){
        $token = generateVerifyToken(60);
        $this->api_token = $token;
        $this->save();
        return $token;
    }

}
