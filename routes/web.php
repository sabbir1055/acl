<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('admin.login');
})->name('login');
Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        return redirect()->route('login');
    })->name('login');
    Route::view('/login', 'admin.login')->name('login');
    Route::post('login', 'AdminController@adminLogin')->name('login.attempt');
//Route::view('/registration', 'admin.registration');
//Route::post('registration', 'AdminController@postRegistration');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    Route::get('/logout', 'AdminController@logout')->name('logout');
//    System user Route
    Route::group(['prefix' => '/system-user'], function () {
        Route::get('/', 'UserController@system_userIndex')->name('system_user.list')->middleware('can:system user list');
        Route::post('/store', 'UserController@system_userStore')->name('system_user.store')->middleware('can:system user create');
        Route::post('/{id}/update', 'UserController@system_userUpdate')->name('system_user.update')->middleware('can:system user update');
    });
    //    Role routes
    Route::group(['prefix' => '/role'], function () {
        Route::get('/', 'RoleController@index')->name('role.index')->middleware('can:role list');
        Route::get('/create', 'RoleController@create')->name('role.create')->middleware('can:role create');
        Route::post('/store', 'RoleController@store')->name('role.store')->middleware('can:role create');
        Route::get('/{id}/edit', 'RoleController@edit')->name('role.edit')->middleware('can:role update');
        Route::post('/{id}/update', 'RoleController@update')->name('role.update')->middleware('can:role update');
    });
//    Permission routes
    Route::group(['prefix' => 'permission'], function () {
        Route::get('/', 'PermissionController@index')->name('permission.index')->middleware('can:permission list');
        Route::post('/store', 'PermissionController@store')->name('permission.store')->middleware('can:permission create');
        Route::post('/{id}/update', 'PermissionController@update')->name('permission.update')->middleware('can:permission update');
    });
//  Profile routes
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'UserController@profile')->name('profile');
        Route::post('/update', 'UserController@profileUpdate')->name('profile.update');
    });

});
Route::post('/division', 'CommonController@division')->name('division.list');
Route::post('/district', 'CommonController@district')->name('district.list');
Route::post('/upazila', 'CommonController@upazila')->name('upazila.list');
Route::get('/user-verify', 'UserController@verify')->name('user.verify');


////clear all cache/////
Route::get('/clear-cache', function () {

    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    // return what you want
    return "cache cleared";

});
