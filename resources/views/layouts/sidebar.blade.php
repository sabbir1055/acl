<aside class="sidebar">
    <div class="sidebar-container">
        <div class="sidebar-header">
            <div class="brand">
                <div class="logo">
                    <!-- Logo -->
                    <span class="l l1"></span>
                    <span class="l l2"></span>
                    <span class="l l3"></span>
                    <span class="l l4"></span>
                    <span class="l l5"></span>
                </div> {{config('app.name')}}
            </div>
        </div>
        <nav class="menu">
            <ul class="sidebar-menu metismenu" id="sidebar-menu">
                <li>
                    <a href="{{url('dashboard')}}"><i class="fe fe-home"></i> <span>Dashboard</span></a>
                </li>
                @canany('permission list','role list','role create', 'system user list')
                    <li class="">
                        <a href="javascript:void(0)"><i class="fe fe-lock"></i> <span> Role Permission  </span> <i class="fa arrow"></i></a>
                        <ul class="sidebar-nav">
                            @can('system user list')
                                <li>
                                    <a href="{{route('system_user.list')}}"><i class="fe fe-users"></i> <span> System users </span></a>
                                </li>
                            @endcan
                            @can('permission list')
                                <li>
                                    <a href="{{route('permission.index')}}"><i class="fe fe-list-task"></i> <span> Permission List</span></a>
                                </li>
                            @endcan
                            @can('role list')
                                <li>
                                    <a href="{{route('role.index')}}"><i class="fe fe-list-task"></i> <span> Role List</span></a>
                                </li>
                            @endcan
                            @can('role create')
                                <li>
                                    <a href="{{route('role.create')}}"><i class="fe fe-plus"></i> <span> Role Create</span></a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
            </ul>
        </nav>
    </div>
    <footer class="sidebar-footer">
        <ul class="sidebar-menu metismenu" id="customize-menu">
            <li>
                <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i> Logout </a>
            </li>
        </ul>
    </footer>
</aside>