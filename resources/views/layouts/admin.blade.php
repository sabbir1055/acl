<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>{{ config('app.name', '') }} || @yield('title')</title>

    @yield('css')
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('/')}}css/vendor.css">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <script type="text/javascript">
        var current_url = "{{ url()->current() }}";
    </script>
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    @stack('css')
</head>

<body>
    <!-- Main Wrapper -->
    <div class="main-wrapper">
        <div class="app" id="app">
            <!-- Header -->
            @include('layouts.header')
            <!-- /Header -->
            <!-- Sidebar -->
            @include('layouts.sidebar')
            <!-- /Sidebar -->
            <!-- Page Wrapper -->
            <div class="page-wrapper mt-3">
                @yield('content')
            </div>
            <!-- /Page Wrapper -->
        </div>
    </div>
    <!-- /Main Wrapper -->
    <!-- jQuery -->
    <script src="{{asset('/')}}js/vendor.js"></script>
    <script src="{{asset('/')}}js/app.js"></script>
    @yield('script')
    <script src="{{ asset('js/custom.js') }}"></script>
    @stack('script')
</body>

</html>