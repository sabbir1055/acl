@extends('layouts.admin')

@section('title','Role List')

@section('content')
    @can('role create')
        <header class="page-title">
            <a href="{{route('role.create')}}">
                <button type="button" class="btn btn-info" id="add-new-btn"><span class="fa fa-plus"></span></button>
            </a>
        </header>
    @endcan
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of Roles</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript:(0);">Role</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                @include('admin.partials.status')
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                <tr class="text-center">
                                    <th>Id</th>
                                    <th>Role Name</th>
                                    <th>Guard Name</th>
                                    {{--<th>Permissions</th>--}}
                                    @can('role update')
                                        <th>Action</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @php $sl = $role->appends($req)->firstItem(); @endphp
                                @forelse ($role as $user)
                                    <tr class="text-center">
                                        <td>{{ $sl++ }}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{ $user->guard_name }}</td>
                                        {{--<td>--}}
                                        {{--@forelse($user->permissions as $value)--}}
                                        {{--<span class="badge badge-success">{{$value->name}}</span>--}}
                                        {{--@empty--}}
                                        {{--<span class="badge badge-danger"> This Role have no permission </span>--}}
                                        {{--@endforelse--}}
                                        {{--</td>--}}
                                        @can('role update')
                                            <td>
                                                <a href="{{route('role.edit',$user->id)}}">
                                                    <button class="btn btn-info">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                </a>
                                            </td>
                                        @endcan
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="7" class="text-center">No user found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <nav aria-label="Page navigation example" class="m-3">
                                <span>Showing {{ $role->appends($req)->firstItem() }} to {{ $role->appends($req)->lastItem() }} of {{ $role  ->appends($req)->total() }} entries</span>
                                <div>{{ $role->appends($req)->render() }}</div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection