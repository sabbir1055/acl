@extends('layouts.admin')
@section('title','Dashboard')
@section('content')
	<div class="content container-fluid">
		<!-- Page Header -->
		<div class="page-header">
			<div class="row">
				<div class="col-sm-12 text-center mb-5">
					<h3 class="page-title">Welcome {{auth()->user()->name}}!</h3>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body order-list">
                        
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <!--end col-->
            </div>
        </div>
            <!--end row-->
	</div>
@endsection
