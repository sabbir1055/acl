<!doctype html>
<html class="no-js" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> {{config('app.name')}}  || Login </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.html">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="{{asset('/')}}css/vendor.css">
        <!-- Theme initialization -->
        <link rel="stylesheet" id="theme-style" href="{{asset('/')}}css/app.css">
    </head>
    <body>
        <div class="auth">
            <div class="auth-container">
                <div class="card">
                    <header class="auth-header">
                        <h1 class="auth-title">
                            <div class="logo">
                                <span class="l l1"></span>
                                <span class="l l2"></span>
                                <span class="l l3"></span>
                                <span class="l l4"></span>
                                <span class="l l5"></span>
                            </div> {{config('app.name')}}
                        </h1>
                    </header>
                    <div class="auth-content">
                        @include('admin.partials.status')
                        <p class="text-center">LOGIN TO CONTINUE</p>
                        <form action="{{route('login.attempt')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input class="form-control @error('employee_id') is-invalid @enderror" type="employee_id" value="{{old('employee_id')}}" name="employee_id" id="inputMobile" placeholder="enter your employee id">
                                @error('employee_id')
                                <br>
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group input-group">
                                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="inputPassword" placeholder="Password">
                                <div class="input-group-append see-password">
                                    <span class="input-group-text"><i class="fa fa-eye"></i></span>
                                </div>
                                @error('password')
                                <br>
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-block btn-primary">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="{{asset('/')}}js/vendor.js"></script>
        <script src="{{asset('/')}}js/app.js"></script>

        <script>
            $(document).ready(function () {
                $('.see-password').click(function () {
                    if($(this).prev('input').attr('type') === 'text'){
                        $(this).empty().html(`<span class="input-group-text"><i class="fa fa-eye"></i></span>`)
                        $(this).prev('input').attr('type','password')
                    }else{
                        $(this).empty().html(`<span class="input-group-text"><i class="fa fa-eye-slash"></i></span>`)
                        $(this).prev('input').attr('type','text')
                    }
                })
            })
        </script>
    </body>
</html>