@extends('layouts.admin')

@section('title','System User List')

@section('content')
    @can('system user create')
        <header class="page-title">
            <button type="button" class="btn btn-info" id="add-new-btn"><span class="fa fa-plus"></span></button>
        </header>
    @endcan
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of System Users</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript:(0);">System Users</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                @include('admin.partials.status')
                <div class="card">
                    <div class="card-body">
                        <form action="">
                            <div class="row">
                                <div class="col-xl-2">
                                    <div class="form-group">
                                        {!! Form::label('f_name','Name')!!}
                                        {!! Form::text('f_name', @$req['f_name'], ['class'=>'form-control mb-3','placeholder' => 'Name','id'=>'f_name'])!!}
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="form-group">
                                        {!! Form::label('f_msisdn','Mobile No')!!}
                                        {!! Form::text('f_msisdn', @$req['f_msisdn'], ['class'=>'form-control mb-3','placeholder' => 'Mobile no','id'=>'f_msisdn'])!!}
                                    </div>
                                </div>
                                <div class="col-xl-2">
                                    <div class="form-group">
                                        {!! Form::label('f_role','Role')!!}
                                        {!! Form::select('f_role',getSystemRoles(), @$req['f_role'], ['class'=>'form-control mb-3','placeholder'=>'Select role','id'=>'f_role'])!!}
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                                <div class="col-md-2">
                                    <label for="">&nbsp</label>
                                    <button class="btn btn-info btn-block"> Filter  </button>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                <tr class="text-center">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    @can('system user update')
                                        <th>Action</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @php $sl = $users->appends($req)->firstItem(); @endphp
                                @forelse ($users as $user)
                                    <tr class="text-center">
                                        <td>{{ $sl++ }}</td>
                                        <td class="text-left">
                                            <img class="avatar-img rounded-circle" src="{{asset('/uploads').'/users/'.$user->image}}" alt="User Image" width="40px">{{($user->name ?? 'No Name')}}
                                        </td>
                                        <td>{{ $user->msisdn }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ ($user->roles()->first()->name ?? null) }}</td>
                                        <td> @if($user->status == 1) Active @else Inactive @endif </td>
                                        @can('system user update')
                                            <td>
                                                <button data-value="{{$user}}" data-roles="{{$user->roles->first()}}" class="btn btn-primary edit-btn">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>
                                        @endcan
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" class="text-center">No user found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <nav aria-label="Page navigation example" class="m-3">
                                <span>Showing {{ $users->appends($req)->firstItem() }} to {{ $users->appends($req)->lastItem() }} of {{ $users  ->appends($req)->total() }} entries</span>
                                <div>{{ $users->appends($req)->render() }}</div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="relationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="relationLabel">Add Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['url' => '','method'=>'post','id'=>'related_form','enctype'=>"multipart/form-data"]) !!}
                {!! Form::token() !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="form-group">
                                {!! Form::label('name','User name')!!}
                                {!! Form::text('name', '', ['class'=>'form-control mb-3','placeholder' => 'User name','id'=>'name','required'=>true])!!}
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group">
                                {!! Form::label('email','User email')!!}
                                {!! Form::email('email', '', ['class'=>'form-control mb-3','placeholder' => 'User email','id'=>'email'])!!}
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="form-group">
                                {!! Form::label('msisdn','User contact no')!!}
                                {!! Form::text('msisdn', '', ['class'=>'form-control mb-3','placeholder' => 'User contact no','id'=>'msisdn','required'=>true])!!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('image','User Image')!!}
                                {!! Form::file('image', ['class'=>'form-control mb-3','id'=>'image'])!!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('role','Role')!!}
                                {!! Form::select('role', [''=>'Select a role']+getSystemRoles(),null, ['class'=>'form-control mb-3','id'=>'role','required'=>true])!!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary add-btn" onclick="showAlert('','related_form')">Save</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add');
            $('#relationLabel').text('Add User');
            var url = "{{route('system_user.store')}}";
            $('#related_form').attr('action', url);
            $('#related_form').find("input[type=text],input[type=number],input[type=email],input[type=textarea]").val("");
            $("option:selected").prop("selected", false)
            $('#relationModal').modal('show')
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#relationLabel').text('Update User');
            var related_data = $(this).data('value');
            var related_role= $(this).data('roles');
            var url = window.location.pathname+'/'+related_data.id+'/update';
            $('#related_form').attr('action', url);
            $('#name').val(related_data.name)
            $('#email').val(related_data.email)
            $('#image').attr('required',false)
            $('#msisdn').val(related_data.msisdn)
            $('#role').val(related_role.name)
            $('#relationModal').modal('show')
        })
        function changeFDistrict(){
            $('#f_district').trigger('change')
            setTimeout(()=>{
                $('#f_area').val("{{@$req['f_area']}}")
                $('.select2').select2({
                    width: '100%' // need to override the changed default
                })
            },1000)
        }
    </script>
@endpush
