@extends('layouts.admin')

@section('header')
    @parent
@endsection

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Registration Form</h3>
                    <!-- <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active">Horizontal Form</li>
                    </ul> -->
                </div>
            </div>
        </div>
        <!-- /Page Header -->


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Personal Information</h4>
                    </div>
                    <div class="card-body">
                    @include('admin.partials.status')
                    <!-- <h4 class="card-title">Personal Information</h4> -->
                        {!! Form::open(['url'=>route('user.store'),'method'=>'POST','enctype'=>'multipart/form-data']) !!}
                        <div class="row">

                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('full_name','Full Name',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('full_name',old('full_name'),['class'=>'form-control','placeholder' => 'Enter full name here','id'=>'full_name','required'=>true]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('parent_id','Parent Id',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('parent_id',auth()->user()->unique_id,['class'=>'form-control','placeholder' => 'Enter unique Id here','id'=>'parent_id','readonly'=>'true','required'=>true]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('','Designation',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::text('',$designation->name,['class'=>'form-control','id'=>'','disabled'=>true]) !!}
                                        {!! Form::hidden('designation',$designation->id,['required'=>true]) !!}
                                    </div>
                                </div>
                            </div>
                            {{-- make dynamic area --}}
                            @if(auth()->user()->getDesignation->name === 'Super Admin')
                                <div class="col-xl-6">
                                    <div class="form-group row">
                                        {!! Form::label('work_area','Work Area',['class'=>'col-lg-3 col-form-label']) !!}
                                        <div class="col-lg-9">
                                            {{--call Division data--}}
                                            {!! Form::select('work_area',[''=>'Select division']+$area->unique('division')->pluck('division','division')->toArray(),old('work_area'),['class'=>'select form-control','id'=>'work_area','required'=>true]) !!}
                                        </div>
                                    </div>
                                </div>
                            @elseif(auth()->user()->getDesignation->name == 'Divisional Manager')
                                <div class="col-xl-6">
                                    <div class="form-group row">
                                        {!! Form::label('work_area','Work Area',['class'=>'col-lg-3 col-form-label']) !!}
                                        <div class="col-lg-9">
                                            {{--call District data--}}
                                            {!! Form::select('work_area',[''=>'Select district']+$area->unique('district')->pluck('district','district')->toArray(),old('work_area'),['class'=>'select form-control','id'=>'work_area','required'=>true]) !!}
                                        </div>
                                    </div>
                                </div>
                            @elseif(auth()->user()->getDesignation->name == 'Regional Manager')
                                <div class="col-xl-6">
                                    <div class="form-group row">
                                        {!! Form::label('work_area','Work Area',['class'=>'col-lg-3 col-form-label']) !!}
                                        <div class="col-lg-9">
                                            {{--call area data--}}
                                            {!! Form::select('work_area',[''=>'Select area']+$area->unique('area')->pluck('area','area')->toArray(),[],['class'=>'select form-control','id'=>'work_area','required'=>true]) !!}
                                        </div>
                                    </div>
                                </div>
                            @elseif(auth()->user()->getDesignation->name == 'Area Manager')
                                <div class="col-xl-6">
                                    <div class="form-group row">
                                        {!! Form::label('work_area','Work Area',['class'=>'col-lg-3 col-form-label']) !!}
                                        <div class="col-lg-9">
                                            {{--call area data--}}
                                            {!! Form::select('work_area',[''=>'Select area']+$area->unique('area')->pluck('area','area')->toArray(),[],['class'=>'select form-control','id'=>'work_area','required'=>true]) !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('present_address','Present Address',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('present_address','',['class'=>'form-control','placeholder' => 'Enter present address here','id'=>'present_address','rows'=>'4','cols'=>'5']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('permanent_address','Parmanent Address',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::textarea('permanent_address','',['class'=>'form-control','placeholder' => 'Enter permanent address here','id'=>'permanent_address','rows'=>'4','cols'=>'5']) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="col-xl-12">
                                <div class="form-group row">
                                    {!! Form::label('nid','Nid number',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::number('nid',old('nid'),['class'=>'form-control','placeholder' => 'Enter NID here','id'=>'nid','required'=>true]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('email','Email',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::email('email',old('email'),['class'=>'form-control','placeholder' => 'Enter email here','id'=>'email','required'=>false]) !!}
                                    </div>
                                </div>
                            </div>
                            @if(auth()->user()->getDesignation->name === 'Super Admin')
                                <div class="col-xl-6">
                                    <div class="form-group row">
                                        {!! Form::label('msisdn','Mobile No',['class'=>'col-lg-3 col-form-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::number('msisdn',old('msisdn'),['class'=>'form-control','placeholder' => '0178*******','id'=>'msisdn','required'=>true]) !!}
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-xl-6">
                                    <div class="form-group row">
                                        {!! Form::label('msisdn','Mobile No',['class'=>'col-lg-3 col-form-label']) !!}
                                        <div class="col-lg-9">
                                            {!! Form::number('msisdn',old('msisdn'),['class'=>'form-control','placeholder' => '0178*******','id'=>'msisdn','required'=>true]) !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('image_1','Image 1',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::file('image_1',['class'=>'form-control','id'=>'image_1','required'=>true]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group row">
                                    {!! Form::label('image_2','Image 2',['class'=>'col-lg-3 col-form-label']) !!}
                                    <div class="col-lg-9">
                                        {!! Form::file('image_2',['class'=>'form-control','id'=>'image_2','required'=>true]) !!}
                                    </div>
                                </div>
                            </div>


                        </div>

                        <div class="text-center mt-5">
                            <button type="submit" class="btn btn-success btn-lg">Create Own Unique Id</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection