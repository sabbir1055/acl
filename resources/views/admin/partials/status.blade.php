@push('script')
      <script>
            if({{count($errors)}}){
                  Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "{{$errors->first()}}"
                  })
            }
            if("{{session()->has('success')}}"){
                  Swal.fire({
                        icon: 'success',
                        title: 'Success...',
                        text: "{{session()->get('success')}}"
                  })
            }
            if("{{session()->has('error')}}"){
                  Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: "{{session()->get('error')}}"
                  })
            }
      </script>
@endpush