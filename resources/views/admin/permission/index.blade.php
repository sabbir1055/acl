@extends('layouts.admin')

@section('title','Permission List')

@section('content')
    @can('permission create')
        <header class="page-title">
            <button type="button" class="btn btn-info" id="add-new-btn"><span class="fa fa-plus"></span></button>
        </header>
    @endcan
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">List of Permission</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="javascript:(0);">Permission</a></li>
                        <li class="breadcrumb-item active">List</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-sm-12">
                @include('admin.partials.status')
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover table-center mb-0">
                                <thead>
                                <tr class="text-center">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Guard Name</th>
                                    @can('permission update')
                                        <th>Action</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @php $sl = $permission->appends($req)->firstItem(); @endphp
                                @forelse ($permission as $user)
                                    <tr class="text-center">
                                        <td>{{ $sl++ }}</td>
                                        <td>{{($user->name ?? 'No Name')}}</td>
                                        <td>{{ $user->guard_name }}</td>
                                        @can('permission update')
                                            <td>
                                                <button data-value="{{$user}}" class="btn btn-primary edit-btn">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                            </td>
                                        @endcan
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" class="text-center">No permission found</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <nav aria-label="Page navigation example" class="m-3">
                                <span>Showing {{ $permission->appends($req)->firstItem() }} to {{ $permission->appends($req)->lastItem() }} of {{ $permission  ->appends($req)->total() }} entries</span>
                                <div>{{ $permission->appends($req)->render() }}</div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade text-left mt-auto" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel20" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="form-label">PERMISSION</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'permission.store','method'=>'post','id'=>'form-id','enctype'=>'multipart/form-data']) !!}
                {!! Form::token() !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="name">Name</label>
                            {!! Form::text('name','',['class'=>'form-control form-control-sm','id'=>'name','required'=>True]) !!}
                        </div>
                        <div class="col-md-6">
                            <label for="guard">Guard Name</label>
                            {!! Form::text('guard','',['class'=>'form-control form-control-sm','id'=>'guard','required'=>True]) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success add-btn" onclick="showAlert('','form-id')">Add</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#add-new-btn').click(function(){
            $('.add-btn').text('Add');
            $('#form-label').text('Create Permission');
            var url = "{{route('permission.store')}}";
            $('#form-id').attr('action', url);
            $('#form-id').find("input[type=text],input[type=textarea]").val("");
            $('#dataModal').modal({
                show:true,
                // backdrop: false
            })
        })
        $('.edit-btn').click(function(){
            $('.add-btn').text('Update');
            $('#form-label').text('Update Permission');
            var permission_data = $(this).data('value');
            var url = window.location.pathname+'/'+permission_data.id+'/update';
            $('#form-id').attr('action', url);
            $('#name').val(permission_data.name)
            $('#guard').val(permission_data.guard_name)
            $('#dataModal').modal({
                show:true,
                backdrop: false
            })
        })
    </script>
@endsection