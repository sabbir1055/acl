@extends('layouts.admin')

@section('title','Profile')
@push('css')
<style>
    .profile-image-data {
        display: block;
        width: 155px !important;
        height: 155px !important;
    }

    .profile-overlay {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        height: 100%;
        width: 100%;
        opacity: 0;
        transition: .3s ease;
    }

    .profile-image:hover .profile-overlay {
        opacity: 1;
    }

    .profile-icon {
        color: blue;
        font-size: 25px;
        position: absolute;
        top: 10%;
        left: 80%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        text-align: center;
    }

    .profile-overlay>input {
        display: none;
    }

    .profile-overlay img {
        width: 50px;
        cursor: pointer;
    }
</style>
@endpush
@section('content')
<div class="content container-fluid">

    <!-- Page Header -->
    <div class="page-header">
        <div class="row">
            <div class="col">
                <h3 class="page-title">Profile</h3>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /Page Header -->

    <div class="row">
        <div class="col-md-12">
            @include('admin.partials.status')
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="profile-header">
                                <div class="row align-items-center">
                                    <div class="col-auto profile-image ">
                                        <img class="rounded-circle profile-image-data" alt="User Image" src="{{auth()->user()->image}}">
                                        <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            <div class="profile-overlay">
                                                <label for="file-input">
                                                    <i class="fa fa-image profile-icon"></i>
                                                </label>
                                                <input id="file-input" type="file" name="image" onchange="submit()" />
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col ml-md-n2 profile-user-info">
                                        <h4 class="user-name mb-0">{{auth()->user()->name}}</h4>
                                        <h6 class="text-muted">{{auth()->user()->email}}</h6>
                                        <div class="about-text"> <i class="fa fa-phone"></i> {{auth()->user()->mobile}}</div>
                                        <div class="user-Location"><i class="fa fa-map-marker"></i> {{auth()->user()->getInfo->present_address}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profile-menu">
                <ul class="nav nav-tabs nav-tabs-solid">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#per_details_tab">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#password_tab">Password</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content profile-tab-cont">

                <!-- Personal Details Tab -->
                <div class="tab-pane fade show active" id="per_details_tab">

                    <!-- Personal Details -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title d-flex justify-content-between">
                                        <span>Personal Details</span>
                                        <a class="edit-link" data-toggle="modal" href="#edit_personal_details"><i class="fa fa-edit mr-1"></i>Edit</a>
                                    </h5>
                                    <div class="row">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Name</p>
                                        <p class="col-sm-10">{{auth()->user()->name}}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Email ID</p>
                                        <p class="col-sm-10">{{auth()->user()->email}}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-sm-2 text-muted text-sm-right mb-0 mb-sm-3">Mobile</p>
                                        <p class="col-sm-10">{{auth()->user()->mobile}}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-sm-2 text-muted text-sm-right mb-3">Present Address</p>
                                        <p class="col-sm-10 mb-0">{{(auth()->user()->getInfo->present_address ?? 'N\A')}}<br>
                                    </div>
                                    <div class="row">
                                        <p class="col-sm-2 text-muted text-sm-right mb-3">Permanent Address</p>
                                        <p class="col-sm-10 mb-0">{{(auth()->user()->getInfo->parmanent_address ?? 'N\A')}}<br>
                                    </div>
                                </div>
                            </div>

                            <!-- Edit Details Modal -->
                            <div class="modal fade" id="edit_personal_details" aria-hidden="true" role="dialog">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Personal Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('profile.update')}}" method="POST">
                                                @csrf
                                                <div class="row form-row">
                                                    <div class="col-12 col-sm-12">
                                                        <div class="form-group">
                                                            <label>Name</label>
                                                            <input type="text" name="name" class="form-control" value="{{auth()->user()->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label>Email ID</label>
                                                            <input type="email" name="email" class="form-control" value="{{auth()->user()->email}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label>Mobile No</label>
                                                            <input type="mobile" name="mobile" class="form-control" value="{{auth()->user()->mobile}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <h5 class="form-title"><span>Address</span></h5>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Present Address</label>
                                                            <input type="text" class="form-control" name="present_address" value="{{auth()->user()->getInfo->present_address}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Permanent Address</label>
                                                            <input type="text" class="form-control" name="parmanent_address" value="{{auth()->user()->getInfo->parmanent_address}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="submit" class="btn btn-info btn-block">Save Changes</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Edit Details Modal -->

                        </div>


                    </div>
                    <!-- /Personal Details -->

                </div>
                <!-- /Personal Details Tab -->

                <!-- Change Password Tab -->
                <div id="password_tab" class="tab-pane fade">

                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Change Password</h5>
                            <div class="row">
                                <div class="col-md-10 col-lg-6">
                                    <form action="{{route('profile.update')}}" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <label>Old Password</label>
                                            <input type="password" name="old_password" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>New Password</label>
                                            <input type="password" name="new_password" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input type="password" name="confirm_password" class="form-control" required>
                                        </div>
                                        <button class="btn btn-info" type="submit">Save Changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Change Password Tab -->

            </div>
        </div>
    </div>

</div>
@endsection