$(document).ready(function () {
    $('a[href="' + current_url + '"]').parents('li').addClass('active open');
    $('a[href="' + current_url + '"]').parent().parent('ul').css('display','block');
    // $('.select').select2({
    //     width: '100%'
    // });
})

function passwordModal() {
    var old_pass = $('#old_password').val('');
    var new_pass = $('#new_password').val('');
    var confirm_pass = $('#confirm_password').val('');
    $('#password_change_button').attr('disabled', true);
    $('#passwordModal').modal({
        show: true,
        backdrop: false
    });
}

$(document).ready(function () {
    $(".alert-message").delay(10000).fadeOut("slow");
    $('#new_password').keyup(function() {
        var old_pass = $('#old_password').val();
        var new_pass = $(this).val();
        var confirm_pass = $('#confirm_password').val();
        if (new_pass === confirm_pass && new_pass.length > 5 && confirm_pass.length > 5 && old_pass) {
            $(this).css({
                'border-color': 'green'
            });
            $('#confirm_password').css({
                'border-color': 'green'
            });
            $('#password_change_button').attr('disabled', false);
        } else {
            $(this).css({
                'border-color': 'red'
            });
            $('#password_change_button').attr('disabled', true);
            if (confirm_pass) {
                $('#confirm_password').css({
                    'border-color': 'red'
                });
            }
        }
    });
    $('#confirm_password').keyup(function() {
        var old_pass = $('#old_password').val();
        var new_pass = $('#new_password').val();
        var confirm_pass = $(this).val();
        if (new_pass === confirm_pass && new_pass.length > 5 && confirm_pass.length > 5 && old_pass) {
            $(this).css({
                'border-color': 'green'
            });
            $('#new_password').css({
                'border-color': 'green'
            });
            $('#password_change_button').attr('disabled', false);
        } else {
            $(this).css({
                'border-color': 'red'
            });
            $('#password_change_button').attr('disabled', true);
            if (new_pass) {
                $('#new_password').css({
                    'border-color': 'red'
                });
            }
        }
    });
    $('#old_password').keyup(function() {
        var old_pass = $('#old_password').val();
        var new_pass = $('#new_password').val();
        var confirm_pass = $('#confirm_password').val();
        if (new_pass === confirm_pass && new_pass.length > 5 && confirm_pass.length > 5 && old_pass) {
            $(this).css({
                'border-color': 'green'
            });
            $('#password_change_button').attr('disabled', true);
            $('#confirm_password').css({
                'border-color': 'green'
            });
            $('#password_change_button').attr('disabled', false);
        }
    });
})

function showAlert(message,id){
    event.preventDefault();
    Swal.fire({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes '+message
    }).then((result) => {
        if (result.value) {document.getElementById(id).submit();}
    })
}
